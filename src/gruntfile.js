module.exports = function(grunt) {
  grunt.initConfig({
    compass: {
      dist: {
        options: {
          sassDir: 'assets/sass',
          cssDir: 'assets/css',
          environment: 'production',
        },
      },
    },
    watch: {
      css: {
        files: ['assets/sass/*.scss'],
        tasks: ['compass'],
      },
    },
  });
  grunt.loadNpmTasks('grunt-contrib-compass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.registerTask('default', ['compass', 'watch']);
};