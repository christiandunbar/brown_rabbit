// Mobile menu activator
$('.mobile-activator').on('click', function(){
	$('header nav').slideToggle(500);
});

// Document ready Shorthand
$(function(){

	// Automatic change slide every 7 seconds
	setInterval(function() {
		nextSlide();
	}, 7000);

	// Fade in items if page refresh and no scroll
    itemsFade();

	// Fade in items on scroll
    $(window).scroll(function() {
        itemsFade(); 
    });

	// Fade in news items one by one
	var articles = [];
	$('article').each(function() {
		articles.push(this);
	});
	TweenMax.staggerFromTo(articles, 1.2, {css:{opacity:'0', transform:'translateY(100px)'}}, {css:{opacity:'1', transform:'translateY(0)'}}, 0.6);
});

// Change to previous slide on click
$('.navigation .previous').on('click', function(){
	previousSlide();
});

// Change to next slide on click
$('.navigation .next').on('click', function(){
	nextSlide();
});

// Next Slide function
function nextSlide() {
	if( $('.slide-active').next('img').length === 1 ) {
		$('.slide-active')
		.removeClass('slide-active')
		.next()
		.addClass('slide-active');
	} else {
		$('.slide-active').removeClass('slide-active');
		$('#slider img:first-child').addClass('slide-active');
	}
}

// Previous Slide function
function previousSlide() {
	if( $('.slide-active').prev('img').length === 1 ) {
		$('.slide-active')
		.removeClass('slide-active')
		.prev()
		.addClass('slide-active');	
	} else {
		var lastSlideNr = $('#slider .slide').length - 1
		$('.slide-active').removeClass('slide-active');
		$('#slider .slide:nth('+lastSlideNr+')').addClass('slide-active');
	}
}

// Items fade function
function itemsFade() {
	$('.hide-me').each( function(i){
        var bottom_of_object = $(this).offset().top + $(this).outerHeight();
        var bottom_of_window = $(window).scrollTop() + $(window).height();
        
        if( bottom_of_window > bottom_of_object ){
            TweenMax.fromTo($(this), 1.5, {css:{opacity:'0', transform:'translateY(100px)'}}, {css:{opacity:'1', transform:'translateY(0)'}});
            $(this).removeClass('hide-me');
        }
    });
}

// Nordic BaristaCup Sponsors
var sponsors = [
	{	
		name: 'sponsor-1',
		image: 'assets/img/sponsors/1.png'	,
		link: '#'
	},
	{	
		name: 'sponsor-2',
		image: 'assets/img/sponsors/2.png'	,
		link: '#'
	},
	{	
		name: 'sponsor-3',
		image: 'assets/img/sponsors/3.png'	,
		link: '#'
	},
	{	
		name: 'sponsor-4',
		image: 'assets/img/sponsors/4.png'	,
		link: '#'
	},
	{	
		name: 'sponsor-5',
		image: 'assets/img/sponsors/5.png'	,
		link: '#'
	},
	{	
		name: 'sponsor-6',
		image: 'assets/img/sponsors/6.png'	,
		link: '#'
	},
	{	
		name: 'sponsor-7',
		image: 'assets/img/sponsors/7.png'	,
		link: '#'
	},
	{	
		name: 'sponsor-8',
		image: 'assets/img/sponsors/8.png'	,
		link: '#'
	},
	{	
		name: 'sponsor-9',
		image: 'assets/img/sponsors/9.png'	,
		link: '#'
	},
	{	
		name: 'sponsor-10',
		image: 'assets/img/sponsors/10.png',
		link: '#'	
	},
	{	
		name: 'sponsor-11',
		image: 'assets/img/sponsors/11.png',
		link: '#'	
	},
	{	
		name: 'sponsor-12',
		image: 'assets/img/sponsors/12.png',
		link: '#'	
	},
	{	
		name: 'sponsor-13',
		image: 'assets/img/sponsors/13.png',
		link: '#'	
	},
	{	
		name: 'sponsor-14',
		image: 'assets/img/sponsors/14.png',
		link: '#'	
	},
	{	
		name: 'sponsor-15',
		image: 'assets/img/sponsors/15.png',
		link: '#'	
	},
	{	
		name: 'sponsor-16',
		image: 'assets/img/sponsors/16.png',
		link: '#'	
	},
	{	
		name: 'sponsor-17',
		image: 'assets/img/sponsors/17.png',
		link: '#'	
	},
	{	
		name: 'sponsor-18',
		image: 'assets/img/sponsors/18.png',
		link: '#'	
	},
	{	
		name: 'sponsor-19',
		image: 'assets/img/sponsors/19.png',
		link: '#'	
	},
	{	
		name: 'sponsor-20',
		image: 'assets/img/sponsors/20.png',
		link: '#'	
	},
	{	
		name: 'sponsor-21',
		image: 'assets/img/sponsors/21.png',
		link: '#'	
	},
	{	
		name: 'sponsor-22',
		image: 'assets/img/sponsors/22.png',
		link: '#'	
	}
]

// Append each sponsor into the sponsor-grid
sponsors.forEach(function(sponsor) {
	var sponsorItem = '<div class="grid-item"><a href="'+sponsor.link+'" target="_blank"><img src="'+sponsor.image+'" alt="'+sponsor.name+'" title="'+sponsor.name+'"></a></div>';
	$('.sponsor-grid').append(sponsorItem);
});

// Images for Flickr Stream
var flickrPhotos = [
	{	
		name: 'flickr-1',
		image: 'assets/img/flickr/1.jpg'	
	},
	{	
		name: 'flickr-2',
		image: 'assets/img/flickr/2.jpg'	
	},
	{	
		name: 'flickr-3',
		image: 'assets/img/flickr/3.jpg'	
	},
	{	
		name: 'flickr-4',
		image: 'assets/img/flickr/4.jpg'	
	},
	{	
		name: 'flickr-5',
		image: 'assets/img/flickr/5.jpg'	
	},
	{	
		name: 'flickr-6',
		image: 'assets/img/flickr/6.jpg'	
	},
	{	
		name: 'flickr-7',
		image: 'assets/img/flickr/7.jpg'	
	},
	{	
		name: 'flickr-8',
		image: 'assets/img/flickr/8.jpg'	
	},
	{	
		name: 'flickr-9',
		image: 'assets/img/flickr/9.jpg'	
	}
]

// Append each flickr photo into the flickr stream
flickrPhotos.forEach(function(flickrPhoto) {
	var flickrItem = '<div class="flickr-item"><img src="'+flickrPhoto.image+'" alt="'+flickrPhoto.name+'"></div>';
	$('.flickr-stream').append(flickrItem);
});